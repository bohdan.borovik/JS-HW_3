console.table(countries);

/* Домашка
1. Отрендерить таблицу стран. в таблицу должны войти поля: name, capital, region, area
2. добавить поисковик (по примеру с занятия), событие на него
3. по событию искать частичные совпадения введенного текста с полями name, capital, region. выдавать полученные результаты в таблицу
4. если нет результатов - показывать соответствующую строку "не найдено" в результатах поиска
*/

function renderCountries(countries) {
    const htmlStr = countries.reduce(function(acc, country, index) {
        return acc + `<tr><td>${index + 1}</td><td>${country.name}</td><td>${country.capital}</td><td>${country.region}</td></tr>`;
    }, '');
    document.querySelector('tbody').innerHTML = htmlStr || `<tr><td colspan="4" class="text-center">Not Found</td></tr>`;
}
renderCountries(countries);

let searchInput = document.querySelector('#search');

searchInput.onkeyup = function(event) {
    let value = event.currentTarget.value.toLowerCase().trim();
    let filteredCountries = countries.filter(function(country) {
        if(country.name.toLowerCase().includes(value)) {
            return country.name.toLowerCase().includes(value);
        } else if(country.region.toLowerCase().includes(value)) {
            return country.region.toLowerCase().includes(value);
        } else if(country.capital.toLowerCase().includes(value)) {
            return country.capital.toLowerCase().includes(value);
        }
    });
    renderCountries(filteredCountries);
}